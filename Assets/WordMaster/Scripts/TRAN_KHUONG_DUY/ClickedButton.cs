﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TRAN_KHUONG_DUY
{
    public class ClickedButton : MonoBehaviour
    {
        public RectTransform rectTransform;
        public Sprite spriteDown;
        public Sprite spriteUp;
        public Image buttonImg;
        public float posDown;
        public float posUp;

        public void Pressed()
        {
            buttonImg.sprite = spriteDown;
            buttonImg.SetNativeSize();
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, posDown);
        }

        public void Released()
        {
            buttonImg.sprite = spriteUp;
            buttonImg.SetNativeSize();
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, posUp);
        }
    }
}
