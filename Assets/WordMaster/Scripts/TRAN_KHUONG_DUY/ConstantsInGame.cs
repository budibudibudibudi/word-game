﻿
namespace TRAN_KHUONG_DUY
{
    public class ConstantsInGame
    {
        #region TAGS

        public const string Tag_0 = "3 Words";
        public const string Tag_1 = "4 Words";
        public const string Tag_2 = "5 Words";
        public const string Tag_3 = "7 Words";

        #endregion

        #region Keys of PlayerPrefs

        public const string Dictionary_Json = "DictionaryJson";
        public const string Coin = "Coin";
        public const string Background = "Background";
        public const string BGM = "BGM";
        public const string SFX = "SFX";
        public const string Chest = "chest";

        #endregion

        #region Animation names

        public const string Hint_Anim = "hint_appear";
        public const string Result_Anim = "result_appear";

        #endregion
    }
}
