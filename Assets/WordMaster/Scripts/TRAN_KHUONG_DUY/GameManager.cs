﻿using com.adjust.sdk;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TRAN_KHUONG_DUY
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        public delegate void ShowResults(string str);
        public delegate void Hint();
        public delegate bool Undo();

        public event ShowResults OnShowResults;
        public event Hint OnHint;
        public event Undo OnUndo;

        [Header("UI")]
        public GameObject homeUI;
        public GameObject spinWheelUI;
        public GameObject settingsUI;
        public GameObject packageUI;
        public GameObject packageLevelsUI;
        public GameObject lowestTierInGameUI;
        public GameObject highestTierInGameUI;
        public GameObject fadeImgUI;
        public GameObject resultBoardUI;
        public GameObject coinShopUI;
        public GameObject panel_tutorial_UI;
        public GameObject ads_layer_UI;

        public TMP_Text homeCointText;
        public TMP_Text inGameCointText;
        public TMP_Text packageLable;
        public TMP_Text levelsLabel;
        public TMP_Text inGameLabel;

        public GameObject title;

        [Space]
        public GameObject playButton1;
        public GameObject playButton2;
        public GameObject bg1Check;
        public GameObject bg2Check;

        [Space]
        public Animation[] victoryNotification;
        private Animation lastVictoryNotification;

        [Space]
        public LevelScript[] levels;

        [Space]
        public GameObject menuBackground;
        public GameObject inGameBackground;
        public SpriteRenderer menuBackgroundSpr;
        public Sprite homeTile_1;
        public Sprite homeTile_2;

        [Space]
        public Image BGMIcon;
        public GameObject BGMCheck;
        public Sprite BGMOn;
        public Sprite BGMOff;
        public Image SFXIcon;
        public GameObject SFXCheck;
        public Sprite SFXOn;
        public Sprite SFXOff;

        [Header("In Game")]
        public CURRENTGROUP currentGroup = CURRENTGROUP.G_3;
        public string currentContent;
        public LevelGenerator levelGenerator;

        [Space(10)]
        public TMP_Text drawingStringTxt;
        public TMP_Text qualityTxt;
        public GameObject emptyStar1;
        public GameObject emptyStar2;
        public GameObject bigEmptyStar2;
        public GameObject emptyStar3;
        public GameObject resultStar1;
        public GameObject resultStar2;
        public GameObject resultBigStar2;
        public GameObject resultStar3;
        public TMP_Text moveTxt;
        public TMP_Text previousRecordTxt;
        public TMP_Text label_result;

        [Header("Packages")]
        public Packages[] packagesArray;

        private string[] contentParagraphs;
        private int moveNumber;
        private int resultNumber;

        public int levelNumber;
        
        //[SerializeField] private LevelStatus[] levelStatus;
        [SerializeField] private LevelContent[] levelContents;
        private bool isCalculating = false;

        [SerializeField] private GameObject widget_coinKurang;
        [SerializeField] private GameObject widget_hintHabis;

        private int coin = 0 , star = 0;
        private bool isFromMenu = true;
        public int GetCoinValue() { return coin; }

        private packageManager.PackageLevel currentPackage;

        public bool EndGame { get; set; }

        public int BGMStatus { get; private set; }
        public int SFXStatus { get; private set; }

        private GameServices gameServices;
        private string currentPackageName;
        private int currentHint;

        //for button ads hint
        [SerializeField] private GameObject[] adsContents;
        [SerializeField] private TMP_Text label_countdown;

        [SerializeField] private int countdown = 60;
        [SerializeField] private Button b_freeAds;
        [SerializeField] private bool isAdsWaiting;

        public GameServices GetGameServices()
        {
            gameServices = gameObject.GetComponent<GameServices>();
            return gameServices;
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            if (PlayerPrefs.GetInt("FirstGame") == 0)
            {
                PlayerPrefs.SetInt(ConstantsInGame.Coin, 200);
                homeCointText.text = inGameCointText.text = coin + "";
                PlayerPrefs.SetInt("FirstGame", 1);
            }

            if(PlayerPrefs.HasKey("LevelNumber"))
            {
                levelNumber = PlayerPrefs.GetInt("LevelNumber");
            }
        }

        private void OnEnable()
        {
           AdsManager.Instance.OnAdRewardSuccess += Instance_AdsReward;
        }

        private void OnDisable()
        {
            AdsManager.Instance.OnAdRewardSuccess -= Instance_AdsReward;
        }

        private void Instance_AdsReward(string name)
        {
            if (name == AdsManager.FreeHint)
            {
                if (OnHint != null)
                {
                    OnHint();
                }

                ads_layer_UI.SetActive(false);
            }
        }

        private void Start()
        {
            //PlayerPrefs.DeleteAll();
            Application.targetFrameRate = 60;
            CheckSetup();
            levelGenerator.InitEssentialArrays();
        }

        public void CheckSetup()
        {
            // Check coin
            coin = PlayerPrefs.GetInt(ConstantsInGame.Coin, 0);
            homeCointText.text = inGameCointText.text = coin + "";

            // Check star
            if(PlayerPrefs.HasKey("STARS"))
            {
                star = PlayerPrefs.GetInt("STARS");
            }

            // Check background type
            if (PlayerPrefs.GetInt(ConstantsInGame.Background, 1) == 1)
            {
                playButton1.SetActive(true);
                bg1Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_1;
            }
            else
            {
                playButton2.SetActive(true);
                bg2Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_2;
            }

            // Check music in game
            if (PlayerPrefs.GetInt(ConstantsInGame.BGM, 1) == 1)
            {
                BGMIcon.sprite = BGMOn;
                BGMCheck.SetActive(true);
                BGMStatus = 1;
                SoundManager.Instance.menuBG.Play();
            }
            else
            {
                BGMIcon.sprite = BGMOff;
                BGMCheck.SetActive(false);
                BGMStatus = 0;
            }

            // Check sound in game
            if (PlayerPrefs.GetInt(ConstantsInGame.SFX, 1) == 1)
            {
                SFXIcon.sprite = SFXOn;
                SFXCheck.SetActive(true);
                SFXStatus = 1;
            }
            else
            {
                SFXIcon.sprite = SFXOff;
                SFXCheck.SetActive(false);
                SFXStatus = 0;
            }
        }

        // Settings button is clicked
        public void SettingsBtn_Onclick()
        {
            homeUI.SetActive(false);
            settingsUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Turn off coin shop button is clicked
        public void TurnOffCoinShop()
        {
            if(isFromMenu)
            {
                homeUI.SetActive(true);

                fadeImgUI.SetActive(false);
                coinShopUI.SetActive(false);
            }
            else
            {
                int n = 0;

                if (PlayerPrefs.HasKey("LEVELSAATINI"))
                {
                    n = PlayerPrefs.GetInt("LEVELSAATINI");
                }

                if (n < packageManager.Instance.GetMaxPackageList())
                {
                    string findPackageName = packageManager.Instance.GetPackageName(n);
                    LoadData(packageManager.Instance.GetPackageLevel(findPackageName));
                }

                fadeImgUI.SetActive(false);
                coinShopUI.SetActive(false);
            }
        }

        // More game button is clicked
        public void MoreGameBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Shop button is clicked
        public void ShopBtn_Onclick(bool fromMenu)
        {
            isFromMenu = fromMenu;

            homeUI.SetActive(false);

            menuBackground.SetActive(true);
            inGameBackground.SetActive(false);

            fadeImgUI.SetActive(true);
            coinShopUI.SetActive(true);
            levelGenerator.CloseLevel();
            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Email button is clicked
        public void EmailBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Game center button is clicked
        public void GameCenterBtn_Onclick()
        {
            // Do something

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // BGM button is clicked
        public void BGMButton_Onclick()
        {
            if (BGMCheck.activeInHierarchy)
            {
                PlayerPrefs.SetInt(ConstantsInGame.BGM, 0);
                BGMIcon.sprite = BGMOff;
                BGMCheck.SetActive(false);
                BGMStatus = 0;
                SoundManager.Instance.menuBG.Stop();
            }
            else
            {
                PlayerPrefs.SetInt(ConstantsInGame.BGM, 1);
                BGMIcon.sprite = BGMOn;
                BGMCheck.SetActive(true);
                BGMStatus = 1;
                SoundManager.Instance.menuBG.Play();
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // SFX button is clicked
        public void SFXButton_Onclick()
        {
            if (SFXCheck.activeInHierarchy)
            {
                PlayerPrefs.SetInt(ConstantsInGame.SFX, 0);
                SFXIcon.sprite = SFXOff;
                SFXCheck.SetActive(false);
                SFXStatus = 0;
            }
            else
            {
                PlayerPrefs.SetInt(ConstantsInGame.SFX, 1);
                SFXIcon.sprite = SFXOn;
                SFXCheck.SetActive(true);
                SFXStatus = 1;
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Background button is clicked
        public void BackgroundButton_Onclick(int type)
        {
            PlayerPrefs.SetInt(ConstantsInGame.Background, type);
            if (type == 1)
            {
                playButton1.SetActive(true);
                playButton2.SetActive(false);
                bg1Check.SetActive(true);
                bg2Check.SetActive(false);
                menuBackgroundSpr.sprite = homeTile_1;
            }
            else
            {
                playButton1.SetActive(false);
                playButton2.SetActive(true);
                bg1Check.SetActive(false);
                bg2Check.SetActive(true);
                menuBackgroundSpr.sprite = homeTile_2;
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Play button on Home is clicked
        public void PlayButton_Onclick()
        {
            homeUI.SetActive(false);
            title.SetActive(false);
            packageUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnPlay.Play();
            }
        }

        // Spin Wheel button on Home is clicked
        public void SpinWheelButton_OnClick()
        {
            homeUI.SetActive(false);
            title.SetActive(false);
            spinWheelUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnPlay.Play();
            }
        }

        // Back button on Package is clicked
        public void BackToHomeButton_Onclick()
        {
            homeUI.SetActive(true);
            title.SetActive(true);

            settingsUI.SetActive(false);
            packageUI.SetActive(false);
            spinWheelUI.SetActive(false);
            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);
            
            levelGenerator.CloseLevel();

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void BackToHomeFromSetting()
        {
            homeUI.SetActive(true);
            title.SetActive(true);

            settingsUI.SetActive(false);
            packageUI.SetActive(false);
            spinWheelUI.SetActive(false);
            lowestTierInGameUI.SetActive(false);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void BackToHome()
        {
            BackToHomeButton_Onclick();

            int r = Random.Range(0, 3);
            if (r == 0)
            {
                ads_layer_UI.SetActive(true);
                AdsManager.Instance.RequestInterstitial();
                ads_layer_UI.SetActive(false);
            }
        }
        //
        public void PackageButton_Onclick(string packageLabel, string levelsLabel, LevelContent[] levelContents)
        {
            packageUI.SetActive(false);
            packageLevelsUI.SetActive(true);

            this.packageLable.text = packageLabel;
            this.levelsLabel.text = levelsLabel;

            // Setup levels
            int length = levelContents.Length;
            for (int i = 0; i < length; i++)
            {
                this.levels[i].SetUp(levelContents[i].status.unlock,
                                     levelContents[i].status.starNumber,
                                     levelContents[i].group,
                                     levelContents[i].content);
            }

            // Cache these variables to use later
            this.levelContents = levelContents;

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        // Back button on Package level 2 is clicked
        public void BackToPackageButton_Onclick()
        {
            if(isCalculating)
            {
                return;
            }

            int r = Random.Range(0, 3);
            if (r == 0)
            {
                ads_layer_UI.SetActive(true);
                AdsManager.Instance.RequestInterstitial();
                ads_layer_UI.SetActive(false);
            }

            homeUI.SetActive(false);

            menuBackground.SetActive(true);
            inGameBackground.SetActive(false);

            lowestTierInGameUI.SetActive(false);
            panel_tutorial_UI.SetActive(false);

            packageUI.SetActive(true);
            levelGenerator.CloseLevel();

            if (BGMStatus == 1)
            {
                SoundManager.Instance.menuBG.Play();
                SoundManager.Instance.ingameBG.Stop();
            }

            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }
        }

        public void StartLevel(CURRENTGROUP group, string content, int level , string packageName)
        {
            packageLevelsUI.SetActive(false);
            lowestTierInGameUI.SetActive(true);
            highestTierInGameUI.SetActive(true);
            menuBackground.SetActive(false);
            inGameBackground.SetActive(true);
            packageUI.SetActive(false);

            inGameLabel.text = packageName;

            levelNumber = level;
            PlayerPrefs.SetInt("LevelNumber", level);
            currentGroup = group;
            currentContent = content;
            contentParagraphs = currentContent.Split(' ');
            levelGenerator.InitLevel(currentGroup, currentContent, contentParagraphs);

            if (BGMStatus == 1)
            {
                SoundManager.Instance.menuBG.Stop();
                SoundManager.Instance.ingameBG.Play();
            }
            if (SFXStatus == 1)
            {
                SoundManager.Instance.btnClick.Play();
            }

            if(isAdsWaiting)
            {
                b_freeAds.interactable = false;

                adsContents[1].SetActive(true);
                adsContents[0].SetActive(false);
            }
            else
            {
                b_freeAds.interactable = true;

                adsContents[0].SetActive(true);
                adsContents[1].SetActive(false);
            }
        }

        // Back button on inGame is clicked
        public void BackToLevelButton_Onclick()
        {
            if (!EndGame)
            {
                lowestTierInGameUI.SetActive(false);
                highestTierInGameUI.SetActive(false);
                packageLevelsUI.SetActive(true);
                inGameBackground.SetActive(false);
                menuBackground.SetActive(true);
                panel_tutorial_UI.SetActive(false);

                ResetGame();
                levelGenerator.CloseLevel();

                // Update information of the level
                PackageButton_Onclick(packageLable.text, levelsLabel.text, levelContents);
            }
        }

        public void CheckResults(string drawingString)
        {
            if (drawingString.Length >= 3)
            {
                moveNumber++;
            }

            bool rightAnswer = false;

            int stringLength = contentParagraphs.Length;
            for (int i = stringLength - 1; i >= 0; i--)
            {
                if (drawingString == contentParagraphs[i])
                {
                    if (OnShowResults != null)
                    {
                        OnShowResults(drawingString);
                        resultNumber++;
                        rightAnswer = true;
                    }
                    break;
                }
            }

            if (!rightAnswer)
            {
                if (SFXStatus == 1)
                {
                    SoundManager.Instance.sfxWrongAnswer.Play();
                }
            }
            else
            {
                if (SFXStatus == 1)
                {
                    SoundManager.Instance.sfxWordlistComplete.Play();
                    panel_tutorial_UI.SetActive(false);
                }

                PlayerPrefs.SetString("PEMAINBARU", "new player");
                GetComponent<GameServices>().SetHighscoreButton();

                AdjustEvent adjustEvent = new AdjustEvent("ty29rz");
                Adjust.trackEvent(adjustEvent);
            }

            if (resultNumber == stringLength)
            {
                EndGame = true;
                StartCoroutine(Finishing());
            }
        }

        public IEnumerator Finishing()
        {
            isCalculating = true;
            yield return new WaitForSeconds(1.0f);
            int length = victoryNotification.Length;
            int random = Random.Range(0, length);
            lastVictoryNotification = victoryNotification[random];
            lastVictoryNotification.gameObject.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.sfxFontDown.Play();
            }

            yield return new WaitForSeconds(1.0f);
            lastVictoryNotification.Play("victory_disappear");

            yield return new WaitForSeconds(1.0f);
            lastVictoryNotification.gameObject.SetActive(false);
            fadeImgUI.SetActive(true);
            resultBoardUI.SetActive(true);

            if (SFXStatus == 1)
            {
                SoundManager.Instance.sfxGradeStar.Play();
            }

            // Show current move record used to complete the level
            moveTxt.text = moveNumber + "";

            // Show previous move record used to complete the level
            previousRecordTxt.text = "Previous Record : " + levelContents[levelNumber].status.moveRecord;

            // Save current move record
            levelContents[levelNumber].status.moveRecord = moveNumber;

            emptyStar1.SetActive(true);
            emptyStar2.SetActive(true);
            bigEmptyStar2.SetActive(false);
            emptyStar3.SetActive(true);

            resultStar1.SetActive(false);
            resultStar2.SetActive(false);
            resultBigStar2.SetActive(false);
            resultStar3.SetActive(false);

            PrepareToShowResult();

            // Show result
            int starNumber = 1;
            yield return new WaitForSeconds(0.5f);
            resultStar1.SetActive(true);
            if (moveNumber == resultNumber)
            {
                label_result.text = "You Got <color=white>4</color> star poin";

                starNumber = 4;     // Special - 3 stars with a big star
                AddMoreStars(4);
                yield return new WaitForSeconds(0.5f);
                resultBigStar2.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                resultStar3.SetActive(true);
                if(levelContents[levelNumber].status.starNumber < 4)
                {
                    AddMoreStars(4);
                }
            }
            else if (moveNumber == (resultNumber + 1))
            {
                label_result.text = "You Got <color=white>3</color> star poin";

                starNumber = 3;
                yield return new WaitForSeconds(0.5f);
                resultStar2.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                resultStar3.SetActive(true);
                if (levelContents[levelNumber].status.starNumber < 3)
                {
                    AddMoreStars(3);
                }
            }
            else if (moveNumber == (resultNumber + 2))
            {
                label_result.text = "You Got <color=white>2</color> star poin";
                
                starNumber = 2;
                yield return new WaitForSeconds(0.5f);
                resultStar2.SetActive(true);
                if (levelContents[levelNumber].status.starNumber < 2)
                {
                    AddMoreStars(2);
                }
            }
            if (levelContents[levelNumber].status.starNumber < starNumber)
            {
                levelContents[levelNumber].status.starNumber = starNumber;
            }

            if (levelNumber < 9)
            {
                // We have 5 levels from 1 to 5, and indexes begin from 0 to 4
                levelContents[levelNumber].status.unlock = 1;    // Unlock the next level
            }
            else
            {
                // Notify that we have completed this level - full 5 level statuses
                int packagesArrayLength = packagesArray.Length;
                for (int i = 0; i < packagesArrayLength; i++)
                {
                    if (packageLable.text == packagesArray[i].packageLabel)
                    {
                        for (int j = 0; j < packagesArray[i].packageLevels.Length; j++)
                        {
                            if (levelsLabel.text == packagesArray[i].packageLevels[j].name)
                            {
                                packagesArray[i].packageLevels[j].completeStatus = 1;
                                break;
                            }
                        }
                        break;
                    }
                }

                // Check if we have completed this package or not
                int n = 0;
                for (int i = 0; i < packagesArrayLength; i++)
                {
                    if (packageLable.text == packagesArray[i].packageLabel)
                    {
                        for (int j = 0; j < packagesArray[i].packageLevels.Length; j++)
                        {
                            n += packagesArray[i].packageLevels[j].completeStatus;
                        }

                        if (n >= packagesArray[i].packageLevels.Length)
                        {
                            // unlock the next package
                            PlayerPrefs.SetInt(packagesArray[i + 1].packageLabel, 1);
                        }
                        break;
                    }
                }
            }

            // Save the level data
            /*string levelDataJson = JsonHelper.ToJson(levelStatus);
            PlayerPrefs.SetString(levelsLabel.text, levelDataJson);*/

            packageManager.Instance.SaveData();

            isCalculating = false;
        }

        private void AddMoreStars(int v)
        {
            star += v;
            PlayerPrefs.SetInt("STARS" , star);

            GetComponent<GameServices>().SetNewLeaderboard(star);
        }

        private void PrepareToShowResult()
        {
            if (moveNumber == resultNumber)
            {
                qualityTxt.text = "Perfect!";
                emptyStar2.SetActive(false);
                bigEmptyStar2.SetActive(true);
            }
            else if (moveNumber == (resultNumber + 1))
            {
                qualityTxt.text = "Great!";
            }
            else
            {
                qualityTxt.text = "Good!";
            }
#if ADS_PLUGIN
            AdsControl.Instance.showAds();
#endif
        }

        // Next btn is clicked
        public void NextBtn_Onclick()
        {
            if (!EndGame)
            {
                if (levelNumber < 9)
                {
                    // If the next level was unlocked
                    if (levelContents[levelNumber].status.unlock == 1)
                    {
                        ResetGame();
                        // Update information of the current level
                        levels[levelNumber].UpdateInformation(levelContents[levelNumber].status.unlock,
                                             levelContents[levelNumber].status.starNumber);
                        // Move to the next
                        
                        AdjustEvent adjustEvent = new AdjustEvent("s874yt");
                        Adjust.trackEvent(adjustEvent);

                        levelNumber++;

                        Reshuffle();
                        StartLevel(levelContents[levelNumber].group, levelContents[levelNumber].content, levelNumber , currentPackageName);
                    }
                    else
                    {
                        LevelBtn_Onclick();
                    }
                }
                else
                {
                    // Update information of the current level
                    levels[levelNumber].UpdateInformation(levelContents[levelNumber].status.unlock,
                                         levelContents[levelNumber].status.starNumber);
                    //LevelBtn_Onclick();

                    ResetGame();
                    //levelNumber = 0;

                    int n = 0;
                    if(PlayerPrefs.HasKey("LEVELSAATINI"))
                    {
                        n = PlayerPrefs.GetInt("LEVELSAATINI");
                    }
                    n++;

                    MoreCoin(50);

                    PlayerPrefs.SetInt("LEVELSAATINI", n);

                    //check next package
                    if (n < packageManager.Instance.GetMaxPackageList())
                    {
                        string findPackageName = packageManager.Instance.GetPackageName(n);
                        packageManager.PackageLevel currentPackage = packageManager.Instance.GetPackageLevel(findPackageName);

                        levelNumber = 0;
                        PlayerPrefs.SetInt("LevelNumber", 0);

                        levelContents = currentPackage.packages;
                        //levelStatus = currentPackage.statuses;

                        StartLevel(levelContents[levelNumber].group, levelContents[levelNumber].content, levelNumber , currentPackage.packageName);
                        inGameLabel.text = currentPackage.packageName;

                        ads_layer_UI.SetActive(true);
                        AdsManager.Instance.RequestInterstitial();
                        ads_layer_UI.SetActive(false);
                    }
                    else
                    {
                        BackToPackageButton_Onclick();
                    }
                    
                }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
                currentHint = 0;
            }
        }

        private void Reshuffle()
        {
            // Knuth shuffle algorithm :: courtesy of Wikipedia :)
            for (int t = 0; t < 9; t++)
            {
                if(t > levelNumber - 1)
                {
                    LevelContent tmp = levelContents[t];
                    int r = Random.Range(t, levelContents.Length);
                    levelContents[t] = levelContents[r];
                    levelContents[r] = tmp;
                }
            }
        }

        // Undo btn is clicked
        public void UndoBtn_Onclick()
        {
            if (!EndGame)
            {
                if (OnUndo != null)
                {
                    if (OnUndo())
                    {
                        resultNumber--;
                    }
                }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.C))
            {
                MoreCoin(5000);
            }
        }

        // Reset btn is clicked
        public void ResetBtn_Onclick()
        {
            if (!EndGame)
            {
                ResetGame();
                contentParagraphs = currentContent.Split(' ');
                levelGenerator.InitLevel(currentGroup, currentContent, contentParagraphs);

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        // Free btn is clicked
        public void FreeBtn_Onclick()
        {
            if(isAdsWaiting)
            {
                return;
            }

            if (!EndGame)
            {
                if(levelGenerator.hintLength >= currentPackage.packages[levelNumber].content.Length - 1)
                {
                    widget_hintHabis.SetActive(true);
                    return;
                }

                b_freeAds.interactable = false;
                isAdsWaiting = true;

                adsContents[0].SetActive(false);
                adsContents[1].SetActive(true);
                StartCoroutine(AdsCountdown());

                ads_layer_UI.SetActive(true);
                AdsManager.Instance.RequestRewardedAds(AdsManager.FreeHint);
                ads_layer_UI.SetActive(false);

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        IEnumerator AdsCountdown()
        {
            countdown = 60;
            for (int i = 0; i < countdown; i++)
            {
                label_countdown.text = $"00:{countdown}";
                yield return new WaitForSeconds(1f);
                countdown--;
            }
            b_freeAds.interactable = true;
            isAdsWaiting = false;

            adsContents[0].SetActive(true);
            adsContents[1].SetActive(false);
        }

        // Hint btn is clicked
        public void HintBtn_Onclick()
        {
            if (!EndGame)
            {
                if (levelGenerator.hintLength >= currentPackage.packages[levelNumber].content.Length - 1)
                {
                    widget_hintHabis.SetActive(true);
                    return;
                }

                if (OnHint != null)
                {
                    if (PlayerPrefs.GetInt(ConstantsInGame.Coin) >= 25)
                    {
                        OnHint();
                        coin -= 25;
                        homeCointText.text = inGameCointText.text = coin + "";
                        PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
                        currentHint++;
                    }
                    else
                    {
                        widget_coinKurang.SetActive(true);
                    }
                }

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnHint.Play();
                }
            }
        }

        public void MoreCoin(int _value)
        {
            coin += _value;
            homeCointText.text = inGameCointText.text = coin + "";
            PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
        }

        // Level btn is clicked
        public void LevelBtn_Onclick()
        {
            if (!isCalculating)
            {
                ResetGame();
                BackToPackageButton_Onclick();

                if (SFXStatus == 1)
                {
                    SoundManager.Instance.btnClick.Play();
                }
            }
        }

        // Replay btn is clicked
        public void ReplayBtn_Onclick()
        {
            if (!isCalculating)
            {
                ResetGame();
                ResetBtn_Onclick();
            }
        }

        // Next btn in result board is clicked
        public void NextBtn2_Onclick()
        {
            if (!isCalculating)
            {
                EndGame = false;
                NextBtn_Onclick();
            }
        }

        private void ResetGame()
        {
            fadeImgUI.SetActive(false);
            resultBoardUI.SetActive(false);
            isCalculating = false;
            EndGame = false;
            resultNumber = moveNumber = 0;
            levelGenerator.RemoveUndoActions();
        }

        public void SetCoinValue(int value)
        {
            coin = value;
            PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
            homeCointText.text = inGameCointText.text = coin + "";
        }

        public void LoadData(packageManager.PackageLevel newCurrentPackage)
        {
            levelContents = newCurrentPackage.packages;

            if(levelNumber > 0)
            {
                Reshuffle();
            }

            StartLevel(levelContents[levelNumber].group, levelContents[levelNumber].content, levelNumber, newCurrentPackage.packageName);
            inGameLabel.text = newCurrentPackage.packageName;

            this.currentPackage = newCurrentPackage;
            currentPackageName = newCurrentPackage.packageName;

            //Debug.Log(currentPackage.packageName + " , " + currentPackage.packages[levelNumber - 1].content);
            //check if first time playing show tutorial

            if(newCurrentPackage.packageName == "Musics" && newCurrentPackage.packages[levelNumber].content == "MIC")
            {
                if (!PlayerPrefs.HasKey("PEMAINBARU"))
                {
                    panel_tutorial_UI.SetActive(true);
                    packageManager.Instance.HapusData();
                }
            }
        }

        public void LoadData(string levelsLabel)
        {
            // Load package data
            string levelDataJson = PlayerPrefs.GetString(levelsLabel, "");

            // If there is no data exists then we'll initialize
           /* if (levelDataJson == "")
            {
                levelStatus = new LevelStatus[10];      // We have 10 levels
                levelStatus[0] = new LevelStatus(1);   // Always unlock the first level      
                levelStatus[1] = new LevelStatus(0);
                levelStatus[2] = new LevelStatus(0);
                levelStatus[3] = new LevelStatus(0);
                levelStatus[4] = new LevelStatus(0);
                levelStatus[5] = new LevelStatus(0);
                levelStatus[6] = new LevelStatus(0);
                levelStatus[7] = new LevelStatus(0);
                levelStatus[8] = new LevelStatus(0);
                levelStatus[9] = new LevelStatus(0);
            }
            else
            {
                levelStatus = JsonHelper.FromJson<LevelStatus>(levelDataJson);
                // We have 4 stars if we reach a big star, but in fact just accept 3 stars per level
                int n = (levelStatus[0].starNumber == 4 ? 3 : levelStatus[0].starNumber) +
                        (levelStatus[1].starNumber == 4 ? 3 : levelStatus[1].starNumber) +
                        (levelStatus[2].starNumber == 4 ? 3 : levelStatus[2].starNumber) +
                        (levelStatus[3].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[4].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[5].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[6].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[7].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[8].starNumber == 4 ? 3 : levelStatus[3].starNumber) +
                        (levelStatus[9].starNumber == 4 ? 3 : levelStatus[4].starNumber);
            }*/
        }
    }

    [System.Serializable]
    public class Packages
    {
        public string packageLabel;
        public PackageLevels[] packageLevels;
    }

    [System.Serializable]
    public class PackageLevels
    {
        public string name;
        [HideInInspector]
        public int completeStatus = 0;      // 0 - not complete, 1 - complete
    }
}
