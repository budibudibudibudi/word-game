﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    public class MiniLetterScript : MonoBehaviour
    {
        [SerializeField]
        private Animation anim;

        public char Alphabet { get; set; }

        private SpriteRenderer letterBlockSprite;
        private SpriteRenderer letterSprite;

        public bool IsHint { get; set; }

        private void OnEnable()
        {
            IsHint = false;
            SetTheme();
        }

        private void SetTheme()
        {
            Tema t = ThemeManager.Instance.GetCurrentTheme();
            GetComponent<SpriteRenderer>().sprite = t.LetterContainer;
        }

        // Check if sprites is null
        private void SpriteIsNull()
        {
            if (letterSprite == null)
            {
                letterSprite = GetComponentsInChildren<SpriteRenderer>()[1];
            }
            if (letterBlockSprite == null)
            {
                letterBlockSprite = GetComponent<SpriteRenderer>();
            }
        }

        public void SetLetterSprite(Sprite sprite)
        {
            SpriteIsNull();
            letterSprite.sprite = sprite;
        }

        public void PlayHintAnimation()
        {
            anim.Play(ConstantsInGame.Hint_Anim);
        }

        public void PlayResultAnimation(float delayTime)
        {
            if (!IsHint)
            {
                // Turn off sprites to make delay effect
                SpriteIsNull();
                letterBlockSprite.enabled = false;
                letterSprite.enabled = false;
            }

            Invoke("PlayResult", delayTime);
        }

        private void PlayResult()
        {
            if (!IsHint)
            {
                letterBlockSprite.enabled = true;
                letterSprite.enabled = true;
            }

            anim.Play(ConstantsInGame.Result_Anim);
        }
    }
}
