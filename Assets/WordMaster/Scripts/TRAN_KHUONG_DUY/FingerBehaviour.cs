﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TRAN_KHUONG_DUY
{
    public class FingerBehaviour : MonoBehaviour
    {
        private GameManager gameManager;
        private Camera mainCamera;

        // The list of the letters 
        private List<LetterScript> list;
        private string drawingString;

        private bool touchedScreen = false;

        private void Start()
        {
            gameManager = GameManager.Instance;
            mainCamera = Camera.main;

            list = new List<LetterScript>();
            drawingString = "";
        }

        private void Update()
        {
            if (!gameManager.EndGame)
            {
                if (!gameManager.coinShopUI.activeInHierarchy)
                {
                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        touchedScreen = true;
                    }

                    if (Input.GetKeyUp(KeyCode.Mouse0))
                    {
                        touchedScreen = false;
                        CheckDrawingString();
                    }

                    // Swipe the letters
                    if (touchedScreen)
                    {
                        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                        RaycastHit2D raycastHit2D = Physics2D.Raycast(ray.origin, ray.direction, 10.0f);

                        if (raycastHit2D)
                        {
                            if (list.Count == 0)
                            {
                                // Add a new letter to the list
                                list.Add(raycastHit2D.transform.GetComponent<LetterScript>());
                                list[0].HighLightLetter();
                                list[0].IsPicking = true;

                                // Insert letters into strings
                                drawingString += list[0].Alphabet;

                                if (gameManager.SFXStatus == 1)
                                {
                                    SoundManager.Instance.sfxBlockDrag.Play();
                                }
                            }
                            else
                            {
                                // Check whether the new letter is next to the last letter or not
                                if (list[list.Count - 1].CheckValidity(raycastHit2D.collider))
                                {
                                    LetterScript letterScr = raycastHit2D.transform.GetComponent<LetterScript>();
                                    // If we go back, we will remove the letters in original sequence order
                                    if (list.Contains(letterScr))
                                    {
                                        if (list.Count >= 2)
                                        {
                                            if (letterScr == list[list.Count - 2])
                                            {
                                                list[list.Count - 1].NormalLetter();
                                                list[list.Count - 1].IsPicking = false;
                                                list.RemoveAt(list.Count - 1);

                                                string tmp = drawingString;
                                                drawingString = tmp.Remove(list.Count, 1);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Add a new letter to the list
                                        list.Add(letterScr);
                                        list[list.Count - 1].HighLightLetter();
                                        list[list.Count - 1].IsPicking = true;

                                        // Insert letters into strings
                                        drawingString += list[list.Count - 1].Alphabet;

                                        if (gameManager.SFXStatus == 1)
                                        {
                                            SoundManager.Instance.sfxBlockDrag.Play();
                                        }
                                    }
                                }
                            }
                        }

                        // Update drawing string on screen
                        gameManager.drawingStringTxt.text = drawingString;
                    }
                }
            }
        }

        private void CheckDrawingString()
        {
            if (drawingString != "")
            {
                gameManager.CheckResults(drawingString);

                // Reset the letters state
                int listLength = list.Count;
                for (int i = listLength - 1; i >= 0; i--)
                {
                    list[i].NormalLetter();
                }

                // Clear everything
                list.Clear();
                drawingString = "";

                // Update drawing string on screen
                gameManager.drawingStringTxt.text = drawingString;
            }
        }
    }
}
