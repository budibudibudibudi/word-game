﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TRAN_KHUONG_DUY
{
    public class LevelScript : MonoBehaviour
    {
        private GameManager gameManager;

        public Button button;
        public Image[] stars;
        public GameObject lockIcon;
        public Sprite emptyStar;
        public Sprite normalStar;
        public Sprite bigStar;

        [SerializeField] private CURRENTGROUP group;
        [SerializeField] private string content;

        private void Start()
        {
            gameManager = GameManager.Instance;
        }

        public void SetUp(int isUnlocked, int starNumber, CURRENTGROUP group, string content)
        {
            UpdateInformation(isUnlocked, starNumber);

            this.group = group;
            this.content = content;
        }

        // Level button is clicked
        public void PrepareToPlay(int level)
        {
            gameManager.StartLevel(group, content, level , "TEST");
        }

        public void UpdateInformation(int isUnlocked, int starNumber)
        {
            // Inactive all stars
            int length = stars.Length;
            for (int i = length - 1; i >= 0; i--)
            {
                stars[i].sprite = emptyStar;
                stars[i].SetNativeSize();
                stars[i].rectTransform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }

            if (isUnlocked == 0)
            {
                button.interactable = false;
                stars[0].transform.parent.gameObject.SetActive(false);
                lockIcon.SetActive(true);
            }
            else
            {
                button.interactable = true;
                stars[0].transform.parent.gameObject.SetActive(true);
                lockIcon.SetActive(false);

                if (starNumber < 4)
                {
                    for (int i = 0; i < starNumber; i++)
                    {
                        stars[i].sprite = normalStar;
                    }
                }
                else
                {
                    stars[0].sprite = stars[2].sprite = normalStar;

                    stars[1].sprite = bigStar;
                    stars[1].SetNativeSize();
                    stars[1].rectTransform.localScale = new Vector3(0.35f, 0.35f, 0.35f);
                }
            }
        }
    }
}
