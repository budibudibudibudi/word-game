using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class DailyLoginPage : MonoBehaviour
{
    [System.Serializable]
    public class RewardData
    {
        public RewardEnum tipe = RewardEnum.Coin;
        public Sprite icon;
        public int amount;
    }

    [SerializeField] private RewardData[] allRewards;
    [SerializeField] private BonusCard[] allcards;

    [SerializeField] private Button b_claim , b_close , b_openPanelReward;

    [SerializeField] private GameObject panelReward;
    [SerializeField] private Image iconReward;
    [SerializeField] private TMP_Text labelReward;

    private void Start()
    {
        string saveAwal = DateTime.Now.AddDays(-1).ToShortDateString();

        if (PlayerPrefs.HasKey("LOGINAWAL"))
        {
            saveAwal = PlayerPrefs.GetString("LOGINAWAL");
        }
        else
        {
            PlayerPrefs.SetString("LOGINAWAL", saveAwal);
        }


        //jarak dari awal main sampai hari ini
        DateTime waktuAwal = DateTime.Parse(saveAwal);

        TimeSpan span = DateTime.Now - waktuAwal;
        int jarak = span.Days;
        if(jarak > 5)
        {
            jarak -= 4;
        }

        Init(jarak);

        b_openPanelReward.onClick.AddListener(() =>
        {
            panelReward.SetActive(true);
            iconReward.sprite = allRewards[jarak - 1].icon;

            switch (allRewards[jarak - 1].tipe)
            {
                case RewardEnum.Coin:
                    labelReward.text = $"{allRewards[jarak - 1].amount} Coin.";
                    break;
                case RewardEnum.SpinWheel:
                    labelReward.text = $"Free Spin Wheels!";
                    break;
            }
        });
       
        b_claim.onClick.AddListener(() =>
        {
            allcards[jarak - 1].Claim();
            gameObject.SetActive(false);
        });

        if (allcards[jarak - 1].isBonusClaimed())
        {
            gameObject.SetActive(false);
        }

        b_close.onClick.AddListener(() => gameObject.SetActive(false));
    }

    private void Init(int jarakWaktu)
    {
        for (int i = 0; i < allRewards.Length; i++)
        {
            allcards[i].InitCard(allRewards[i].tipe , allRewards[i].icon, allRewards[i].amount);
        }

        if (jarakWaktu > allcards.Length)
        {
            jarakWaktu = allcards.Length;
        }

        for (int i = 0; i < jarakWaktu; i++)
        {
            allcards[i].ReadyToClaim();
        }
    }

    public void CheckClaimButton(bool aktif)
    {
        b_openPanelReward.interactable = aktif;
    }
}

public enum RewardEnum
{
    Coin,
    SpinWheel
}
