using System;
using System.Collections;
using System.Collections.Generic;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class LockLevel : MonoBehaviour
{
    [SerializeField] private string packageName;

    [SerializeField] private Button b_lock;
    [SerializeField] private int levelPrice;

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        CheckLockLevel();

        b_lock.onClick.AddListener(() =>
        {
            if (levelPrice <= GameManager.Instance.GetCoinValue())
            {
                int sisa = GameManager.Instance.GetCoinValue() - levelPrice;
                GameManager.Instance.SetCoinValue(sisa);

                anim.SetBool("Unlock", true);
                PlayerPrefs.SetInt(packageName, 1);
            }
        });
    }

    private void CheckLockLevel()
    {
        if(!PlayerPrefs.HasKey(packageName))
        {
            PlayerPrefs.SetInt(packageName, 0);
        }

        if(PlayerPrefs.GetInt(packageName) == 1)
        {
            anim.SetBool("Unlock", true);
        }
        else
        {
            PlayerPrefs.SetInt(packageName, 0);
        }
    }
}
