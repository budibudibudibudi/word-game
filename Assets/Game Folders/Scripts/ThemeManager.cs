using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeManager : MonoBehaviour
{
    public static ThemeManager Instance;

    public ThemeHolder holder;
    public Tema[] allThemes;

    public string currentTheme = "Wood";
    public Tema activeTheme;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey("THEMES"))
        {
            currentTheme = PlayerPrefs.GetString("THEMES");
        }
        else
        {
            PlayerPrefs.SetString("THEMES", "Wood");
        }

        SetTheme();
    }

    private void SetTheme()
    {
        Tema findTheme = Array.Find(allThemes, t => t.nama == currentTheme);
        if (findTheme != null)
        {
            activeTheme = findTheme;

            //holder.BGAwal.sprite = findTheme.BG;
            holder.BGGame.sprite = findTheme.BG;
        }
    }

    public void SetNewTheme(string newTheme)
    {
        currentTheme = newTheme;
        PlayerPrefs.SetString("THEMES", newTheme);
        SetTheme();
    }

    public Tema GetCurrentTheme()
    {
        return activeTheme;
    }
}

[System.Serializable]
public class ThemeHolder
{
    public Image BGAwal;
    public SpriteRenderer BGGame;
    public Image ButtonPlay;
    public Image ButtonSetting;
    public Image ButtonLeaderboard;
    public Image ButtonLink;
    public Image ButtonShop;
    public Image ButtonAds;
    public Image ButtonHint;
}
