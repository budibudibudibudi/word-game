using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class packageCard : MonoBehaviour
{
    [SerializeField] private string namaPackage;

    [SerializeField] private GameObject[] packageContent;

    [SerializeField] private TMP_Text label_score;
    [SerializeField] private TMP_Text label_packageName;

    [SerializeField] private bool isBought = true , isDefault;
    [SerializeField] private int price;
    [SerializeField] private TMP_Text label_price;

    [SerializeField] private GameObject panel_layer;
    [SerializeField] private Button b_buy;

    private void Start()
    {
        b_buy.onClick.AddListener(BuyPackage);

        namaPackage = gameObject.name;

        label_packageName.text = gameObject.name;

        packageManager.PackageLevel temp = packageManager.Instance.GetPackageLevel(namaPackage);

        int total = 0;
        for (int i = 0; i < temp.packages.Length; i++)
        {
            total += temp.packages[i].status.starNumber;
        }

        if (PlayerPrefs.HasKey(namaPackage))
        {
            isBought = true;
            packageManager.Instance.AddNewPackage(namaPackage);
            Button thisButton = gameObject.GetComponent<Button>();
            thisButton.onClick.AddListener(() =>
            {
                packageManager.PackageLevel levelThisCard = packageManager.Instance.GetPackageLevel(gameObject.name);
                GameManager.Instance.LoadData(levelThisCard);
            });
        }
        else
        {
            isBought = false;
            SetContentPackage(2);
            label_price.text = price.ToString();
        }

        panel_layer.SetActive(!isBought);

        if(isDefault)
        {
            SetContentPackage(0);
            label_score.text = total.ToString();
            panel_layer.SetActive(false);

            Button thisButton = gameObject.GetComponent<Button>();
            thisButton.onClick.AddListener(() =>
            {
                packageManager.PackageLevel levelThisCard = packageManager.Instance.GetPackageLevel(gameObject.name);
                GameManager.Instance.LoadData(levelThisCard);
            });
        }

        SetCard(total);
    }

    public void SetCard(int score)
    {
        label_score.text = score.ToString();
        if (isBought)
        {
            gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                GameManager.Instance.LoadData(packageManager.Instance.GetPackageLevel(namaPackage));
                GameManager.Instance.levelNumber = 0;
                PlayerPrefs.SetInt("LevelNumber", 0);
            });
        }

        if(score == 40)
        {
            SetContentPackage(1);
        }
    }

    private void SetContentPackage(int v)
    {
        foreach (var p in packageContent)
        {
            p.SetActive(false);
        }
        packageContent[v].SetActive(true);
    }

    public void BuyPackage()
    {
        int coin = PlayerPrefs.GetInt(ConstantsInGame.Coin);

        if(coin < price)
        {
            return;
        }

        isBought = true;
        panel_layer.SetActive(false);
        PlayerPrefs.SetString(namaPackage, "beli");
        packageManager.Instance.AddNewPackage(namaPackage);
        GameManager.Instance.MoreCoin(-price);

        GameManager.Instance.BackToHome();
    }
}