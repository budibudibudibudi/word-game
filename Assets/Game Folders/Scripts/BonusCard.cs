using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class BonusCard : MonoBehaviour
{
    public RewardEnum bonusType;

    [SerializeField] private Image image;
    [SerializeField] private TMP_Text label;
    private int rewardAmount;

    [SerializeField] private bool isClaimed = false, isOpen = false;

    [SerializeField] private GameObject penutup , check;

    [SerializeField] private DailyLoginPage dailyLoginPage;

    public void InitCard(RewardEnum tipe , Sprite gambar , int teks)
    {
        bonusType = tipe;
        image.sprite = gambar;
        rewardAmount = teks;
        label.text = teks.ToString();

        isClaimed = PlayerPrefs.HasKey(gameObject.name);

        penutup.SetActive(!isOpen);
        check.SetActive(isClaimed);
    }

    public bool isBonusClaimed()
    {
        return isClaimed;
    }

    public void ReadyToClaim()
    {
        penutup.SetActive(false);
        isOpen = true;

        if (dailyLoginPage != null)
        {
            dailyLoginPage.CheckClaimButton(!isClaimed);
        }
    }

    public void Claim()
    {
        if(!isOpen)
        {
            return;
        }

        if(isClaimed)
        {
            return;
        }

        switch (bonusType)
        {
            case RewardEnum.Coin:
                GameManager.Instance.MoreCoin(rewardAmount);
                GameManager.Instance.BackToHomeButton_Onclick();
                break;
            case RewardEnum.SpinWheel:
                GameManager.Instance.SpinWheelButton_OnClick();
                break;
        }

        isClaimed = true;
        PlayerPrefs.SetString(gameObject.name, "claim");
    }
}
