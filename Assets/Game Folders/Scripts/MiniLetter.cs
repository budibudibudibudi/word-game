using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniLetter : MonoBehaviour
{
    private void OnEnable()
    {
        SetTheme();
    }
    private void SetTheme()
    {
        Tema t = ThemeManager.Instance.GetCurrentTheme();
        GetComponent<SpriteRenderer>().sprite = t.LetterContainer;
    }
}
