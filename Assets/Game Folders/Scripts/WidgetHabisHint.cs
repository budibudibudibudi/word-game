using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WidgetHabisHint : MonoBehaviour
{
    [SerializeField] private Button b_close;

    private void Start()
    {
        b_close.onClick.AddListener(() => gameObject.SetActive(false));
    }
}
