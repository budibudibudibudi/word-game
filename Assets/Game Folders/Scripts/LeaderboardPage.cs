using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using Unity.Services.Leaderboards.Models;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardPage : MonoBehaviour
{
    [SerializeField] private TMP_Text label_nama;
    [SerializeField] private TMP_Text label_score;
    [SerializeField] private TMP_Text label_peringkat;

    [SerializeField] private GameObject panel_info;
    [SerializeField] private Button b_info;
    [SerializeField] private Button b_back;

    [SerializeField] private TMP_InputField input_nama;

    [SerializeField] private leaderboardCard[] cards;

    private void Start()
    {
        input_nama.onEndEdit.AddListener((string value) =>
        {
            GameManager.Instance.GetComponent<GameServices>().ChangeName(value);
        });

        b_info.onClick.AddListener(() => panel_info.SetActive(true));
        b_back.onClick.AddListener(() => panel_info.SetActive(false));
    }

    private void OnEnable()
    {
        GetLeaderboardBucketData();
    }

    private void GetLeaderboardBucketData()
    {
        GameManager.Instance.GetComponent<GameServices>().LoadAllLeaderboard(OnGetEntry);
    }

    private void OnGetEntry(List<LeaderboardEntry> obj)
    {
        for (int i = 0; i < obj.Count; i++)
        {
            cards[i].gameObject.SetActive(true);
            cards[i].SetLeaderbordData(obj[i].PlayerName, obj[i].Rank + 1, (int)obj[i].Score);
        }
    }

    public void Setleaderboard(string nama, int score, int peringkat)
    {
        input_nama.text = nama;
        label_peringkat.text = $"{peringkat}";
        label_score.text = $"{score}";
    }
}