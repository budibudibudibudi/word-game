using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using TRAN_KHUONG_DUY;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    [SerializeField] private string AppKey_ID;

    //Banner
    [SerializeField] private IronSourceBannerSize bannerSize;
    [SerializeField] private IronSourceBannerPosition bannerPosition;

    //static string
    public static string FreeHint = "Hint";
    public static string DailySpin = "Spin";
    public static string ChangeName = "Name";
    public static string NextLevel = "Next Level";

    public delegate void AdsRewardSuccessDelegate(string name);
    public event AdsRewardSuccessDelegate OnAdRewardSuccess;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

#if UNITY_ANDROID
        string appKey = AppKey_ID;
#elif UNITY_IPHONE
        string appKey = "8545d445";
#else
        string appKey = "unexpected_platform";
#endif
        IronSource.Agent.validateIntegration();
        IronSource.Agent.init(appKey);
    }

    private void OnEnable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent += IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedVideoOnAdRewardedEvent;
        IronSourceRewardedVideoEvents.onAdClickedEvent += IronSourceRewardedVideoEvents_onAdClickedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent += IronSourceInterstitialEvents_onAdClosedEvent;
        IronSourceInterstitialEvents.onAdLoadFailedEvent += IronSourceInterstitialEvents_onAdLoadFailedEvent;
        IronSourceInterstitialEvents.onAdClickedEvent += IronSourceInterstitialEvents_onAdClickedEvent;

        IronSourceEvents.onRewardedVideoAdLoadFailedEvent += IronSourceEvents_onRewardedVideoAdLoadFailedEvent;
    }

    private void IronSourceEvents_onRewardedVideoAdLoadFailedEvent(IronSourceError obj)
    {
        AdjustEvent adjustEvent = new AdjustEvent("q1hhqu");
        Adjust.trackEvent(adjustEvent);
    }

    private void OnDisable()
    {
        IronSourceEvents.onSdkInitializationCompletedEvent -= IronSourceEvents_onSdkInitializationCompletedEvent;
        IronSourceRewardedVideoEvents.onAdRewardedEvent -= RewardedVideoOnAdRewardedEvent;
        IronSourceRewardedVideoEvents.onAdClickedEvent -= IronSourceRewardedVideoEvents_onAdClickedEvent;
        IronSourceInterstitialEvents.onAdClosedEvent -= IronSourceInterstitialEvents_onAdClosedEvent;
        IronSourceInterstitialEvents.onAdLoadFailedEvent -= IronSourceInterstitialEvents_onAdLoadFailedEvent;
        IronSourceInterstitialEvents.onAdClickedEvent -= IronSourceInterstitialEvents_onAdClickedEvent; 
        IronSourceEvents.onRewardedVideoAdLoadFailedEvent -= IronSourceEvents_onRewardedVideoAdLoadFailedEvent;
    }

    private void IronSourceEvents_onSdkInitializationCompletedEvent()
    {
        IronSource.Agent.loadRewardedVideo();
        IronSource.Agent.loadInterstitial();

        IronSource.Agent.loadBanner(IronSourceBannerSize.SMART, bannerPosition);
        IronSource.Agent.displayBanner();
    }

    private void RewardedVideoOnAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
    {
        OnAdRewardSuccess?.Invoke(placement.getPlacementName());
        IronSource.Agent.loadRewardedVideo();

        if (placement.getPlacementName() == DailySpin)
        {
            AdjustEvent adjustEvent = new AdjustEvent("q1uodg");
            Adjust.trackEvent(adjustEvent);
        }
        if (placement.getPlacementName() == FreeHint)
        {
            AdjustEvent adjustEvent = new AdjustEvent("z2vlvc");
            Adjust.trackEvent(adjustEvent);
        }
        if (placement.getPlacementName() == ChangeName)
        {
            AdjustEvent adjustEvent = new AdjustEvent("g6lc8j");
            Adjust.trackEvent(adjustEvent);
        }
    }

    private void IronSourceRewardedVideoEvents_onAdClickedEvent(IronSourcePlacement arg1, IronSourceAdInfo arg2)
    {
        AdjustEvent adjustEvent = new AdjustEvent("z4wv2g");
        Adjust.trackEvent(adjustEvent);
    }

    private void IronSourceInterstitialEvents_onAdClosedEvent(IronSourceAdInfo info)
    {
        IronSource.Agent.loadInterstitial();

        AdjustEvent adjustEvent = new AdjustEvent("inwh5t");
        Adjust.trackEvent(adjustEvent);
    }

    private void IronSourceInterstitialEvents_onAdClickedEvent(IronSourceAdInfo obj)
    {
        AdjustEvent adjustEvent = new AdjustEvent("eq9o92");
        Adjust.trackEvent(adjustEvent);
    }

    private void IronSourceInterstitialEvents_onAdLoadFailedEvent(IronSourceError obj)
    {
        IronSource.Agent.loadInterstitial();

        AdjustEvent adjustEvent = new AdjustEvent("btfnat");
        Adjust.trackEvent(adjustEvent);
    }

    public void RequestRewardedAds(string placement)
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            GameManager.Instance.HintBtn_Onclick();
            return;
        }

        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo(placement);
        }
        else
        {
            RequestRewardedAds(placement);
        }
    }

    public void RequestInterstitial()
    {
        if (PlayerPrefs.HasKey("ADS"))
        {
            return;
        }

        if (IronSource.Agent.isInterstitialReady())
        {
            IronSource.Agent.showInterstitial();
        }
        else
        {
            IronSource.Agent.loadInterstitial();
        }
    }
}
