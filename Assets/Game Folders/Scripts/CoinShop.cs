using com.adjust.sdk;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class CoinShop : MonoBehaviour
{
    [SerializeField] private GameObject panel_loading;
    [SerializeField] private TMP_Text label_coin;

    //for theme store
    [SerializeField] private Button[] b_theme;

    [System.Serializable]
    public class ThemeStore
    {
        public string nama;
        public Button b_use;

        public bool isBought = false;
    }

    [SerializeField] private ThemeStore[] themeStores;

    private void Start()
    {
        SetCoinLabel();

        for (int i = 0; i < themeStores.Length; i++)
        {
            if(themeStores[i].isBought)
            {
                b_theme[i].GetComponentInChildren<TMP_Text>().text = "Use";
            }
            else
            {
                b_theme[i].GetComponentInChildren<TMP_Text>().text = "Buy";
            }

            //delete this if theme available
            b_theme[i].interactable = themeStores[i].isBought;

        }

        themeStores[0].b_use.onClick.AddListener(() =>
        {
            ThemeManager.Instance.SetNewTheme(themeStores[0].nama);
        });

        themeStores[1].b_use.onClick.AddListener(() =>
        {
            ThemeManager.Instance.SetNewTheme(themeStores[1].nama);
        });

        themeStores[2].b_use.onClick.AddListener(() =>
        {
            ThemeManager.Instance.SetNewTheme(themeStores[2].nama);
        });

    }

    private void BuyTheme(int themeNumber)
    {
        int coin = PlayerPrefs.GetInt(ConstantsInGame.Coin);
        if(coin < 1000)
        {
            return;
        }

        //buy the theme
        GameManager.Instance.MoreCoin(-1000);
    }

    private void SetCoinLabel()
    {
        label_coin.text = PlayerPrefs.GetInt(ConstantsInGame.Coin).ToString();
    }

    public void Buy()
    {
        panel_loading.SetActive(true);

        StartCoroutine(Loading());
    }

    IEnumerator Loading()
    {
        yield return new WaitForSeconds(3f);
        panel_loading.SetActive(false);
    }

    public void OnSuccessPurchasing(int n)
    {
        if (n == 50)
        {
            GameManager.Instance.MoreCoin(50);
            SetCoinLabel();
            AdjustEvent adjustEvent = new AdjustEvent("jw5ddy");
            Adjust.trackEvent(adjustEvent);
        }
        if (n == 100)
        {
            GameManager.Instance.MoreCoin(100);
            SetCoinLabel();
            AdjustEvent adjustEvent = new AdjustEvent("7w40ar");
            Adjust.trackEvent(adjustEvent);
        }
        if (n == 1000)
        {
            //remove all ads
            PlayerPrefs.SetString("ADS", "non active");
            AdjustEvent adjustEvent = new AdjustEvent("hhqezc");
            Adjust.trackEvent(adjustEvent);
        }
    }

    public void OnfailedPurchasing()
    {
        AdjustEvent adjustEvent = new AdjustEvent("n9ar7l");
        Adjust.trackEvent(adjustEvent);
    }
}
