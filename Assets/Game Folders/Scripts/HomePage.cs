using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class HomePage : MonoBehaviour
{
    [SerializeField] private Button b_link;
    [SerializeField] private Button b_misteryChest;
    [SerializeField] private Button b_playGame;
    [SerializeField] private Button b_exit;
    [SerializeField] private Button b_shop;
    [SerializeField] private Button b_setting;

    [SerializeField] private TMP_Text label_chest;

    [SerializeField] private string uri;

    [SerializeField] private string tanggal;
    [SerializeField] private DateTime saveTime;

    private bool dailyBonusIsActive = true;
    [SerializeField] private Sprite[] chests;

    [SerializeField] private GameObject panel_speedUpChest;
    [SerializeField] private TMP_Text label_countdown;
    [SerializeField] private Button b_close, b_speedUp;

    private void OnEnable()
    {
        CheckHourly();

        if(GameManager.Instance != null)
        {
            GameManager.Instance.CheckSetup();
        }
        AdsManager.Instance.OnAdRewardSuccess += Instance_OnAdRewardSuccess;
    }

    private void OnDisable()
    {
        AdsManager.Instance.OnAdRewardSuccess -= Instance_OnAdRewardSuccess;
    }

    private void Start()
    {
        b_playGame.onClick.AddListener(() => 
        {
            int n = 0;

            if (PlayerPrefs.HasKey("LEVELSAATINI"))
            {
                n = PlayerPrefs.GetInt("LEVELSAATINI");
            }

            if(n < packageManager.Instance.GetMaxPackageList())
            {
                string findPackageName = packageManager.Instance.GetPackageName(n);
                GameManager.Instance.LoadData(packageManager.Instance.GetPackageLevel(findPackageName));
                gameObject.SetActive(false);
            }
            else
            {
                GameManager.Instance.BackToPackageButton_Onclick();
            }
        });

        b_link.onClick.AddListener(() =>
        {
            Application.OpenURL(uri);
        });

        CheckHourly();
        GameManager.Instance.CheckSetup();

        b_misteryChest.onClick.AddListener(() => 
        {
            //AdsManager.Instance.RequestRewardedAds(AdsManager.DailySpin);
            if(dailyBonusIsActive)
            {
                GameManager.Instance.SpinWheelButton_OnClick();
                CheckHourly();
            }
            else
            {
                panel_speedUpChest.SetActive(true);
            }
            
        });

        b_exit.onClick.AddListener(() => Application.Quit());
        b_shop.onClick.AddListener(() => GameManager.Instance.ShopBtn_Onclick(true));
        b_setting.onClick.AddListener(() => GameManager.Instance.SettingsBtn_Onclick());

        b_close.onClick.AddListener(() => panel_speedUpChest.SetActive(false));
        b_speedUp.onClick.AddListener(() =>
        {
            AdsManager.Instance.RequestRewardedAds("SPEEDUP");
        });
    }

    private void Instance_OnAdRewardSuccess(string name)
    {
        if(name == "SPEEDUP")
        {
        panel_speedUpChest.SetActive(false);
        saveTime = DateTime.Parse(tanggal);

        saveTime = saveTime.AddMinutes(-30);

        tanggal = saveTime.ToString();
        PlayerPrefs.SetString(ConstantsInGame.Chest, tanggal);
        CheckHourly();
        }
    }

    public void CheckHourly()
    {
        saveTime = DateTime.Now.AddHours(-2);

        if(PlayerPrefs.HasKey(ConstantsInGame.Chest))
        {
            tanggal = PlayerPrefs.GetString(ConstantsInGame.Chest);
            saveTime = DateTime.Parse(tanggal);
        }

        DateTime nextDate = saveTime.AddHours(2);
        DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

        TimeSpan span = nextDate - sekarang;
        dailyBonusIsActive = span.Hours <= 0 && span.Minutes <= 0;

        if(dailyBonusIsActive)
        {
            b_misteryChest.image.sprite = chests[0];
            b_misteryChest.GetComponent<Animator>().SetBool("isActive", true);
            label_chest.text = "Claim Reward !";
        }
        else
        {
            b_misteryChest.image.sprite = chests[1];
            b_misteryChest.GetComponent<Animator>().SetBool("isActive", false);
        }
        //b_misteryChest.interactable = dailyBonusIsActive;
    }

    private void Update()
    {
        if(!dailyBonusIsActive)
        {
            saveTime = DateTime.Parse(tanggal);
            DateTime nextDate = saveTime.AddHours(2);

            DateTime sekarang = DateTime.Parse(DateTime.Now.ToString());

            TimeSpan span = nextDate - sekarang;
            label_chest.text = span.ToString();
            label_countdown.text = span.ToString();
        }
    }
}