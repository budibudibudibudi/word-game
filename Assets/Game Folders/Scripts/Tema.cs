using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Themes" , menuName = "New/Themes")]
public class Tema : ScriptableObject
{
    public string nama;

    public Sprite BG;

    public Sprite ButtonPlay;
    public Sprite ButtonSetting;
    public Sprite ButtonShop;
    public Sprite ButtonLink;
    public Sprite ButtonLeaderboard;
    public Sprite ButtonAds;
    public Sprite ButtonHint;

    public Sprite LetterContainer;
}
