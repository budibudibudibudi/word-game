using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TRAN_KHUONG_DUY;
using UnityEngine;

public class packageManager : MonoBehaviour
{
    public static packageManager Instance;

    [System.Serializable]
    public class PackageLevel
    {
        public string packageName;
        public LevelContent[] packages;
        //public LevelStatus[] statuses;
    }

    [SerializeField] private List<string> allPackagesName = new List<string>();
    [SerializeField] private PackageLevel[] allPackages;
    [SerializeField] private PackageLevel[] loadPackages;
 
    [SerializeField] private string path;

    [System.Serializable]
    public class SaveFile
    {
        public List<PackageLevel> levels = new List<PackageLevel>();

        public SaveFile(PackageLevel package)
        {
            levels.Add(package);
        }

        public SaveFile(PackageLevel[] levels)
        {
            this.levels = levels.ToList();
        }
    }


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        path = Application.persistentDataPath + "/FileData.json";

        LoadData();
    }

    public PackageLevel GetPackageLevel(string nama)
    {
        PackageLevel pack = Array.Find(allPackages, p => p.packageName == nama);
        return pack;
    }

    public void SaveData()
    {
        SaveFile temp = new SaveFile(allPackages);
        string json = JsonUtility.ToJson(temp);
        File.WriteAllText(path, json);
    }

    public void LoadData()
    {
        if(!File.Exists(path))
        {
            return;
        }
        
        string json = File.ReadAllText(path);
        SaveFile temp = JsonUtility.FromJson<SaveFile>(json);
        allPackages = temp.levels.ToArray();
    }

    public void HapusData()
    {
        string path = Application.persistentDataPath;
        DirectoryInfo directoryInfo = new DirectoryInfo(path);

        foreach (FileInfo file in directoryInfo.GetFiles())
        {
            file.Delete();

        }
    }

    public string GetPackageName(int n)
    {
        return allPackagesName[n];
    }

    public int GetMaxPackageList()
    {
        return allPackagesName.Count;
    }

    public void AddNewPackage(string newPackage)
    {
        allPackagesName.Add(newPackage);
    }
}
