using System;
using System.Collections;
using System.Collections.Generic;
using EasyUI.PickerWheelUI;
using TMPro;
using TRAN_KHUONG_DUY;
using UnityEngine;
using UnityEngine.UI;

public class SpinWheelPage : MonoBehaviour
{
    [SerializeField] private Button b_spin;
    [SerializeField] private Button b_back;
    [SerializeField] private PickerWheel pickerWheel;

    [SerializeField] private GameObject panel_result;
    [SerializeField] private TMP_Text label_result;

    [SerializeField] private Animator buttonAnim;

    [SerializeField] private bool isAndroid = false;

    private void Start()
    {
        b_back.onClick.AddListener(() => GameManager.Instance.BackToHomeFromSetting());
        panel_result.SetActive(false);
        buttonAnim = b_spin.GetComponent<Animator>();
        buttonAnim.SetBool("isActive", true);

        b_spin.onClick.AddListener(HandleSpin);

        pickerWheel.OnSpinEnd(OnEndSpin);
        label_result.gameObject.SetActive(false);
    }

    private void OnEndSpin(WheelPiece pieces)
    {
        //ShowResult($"<size=50>Congratulation!</size> <br> You Get <size=35><color=green>{pieces.Amount}</color> Coin!!");
        b_spin.interactable = true;

        int coin = PlayerPrefs.GetInt(ConstantsInGame.Coin, 0);
        coin += pieces.Amount;

        PlayerPrefs.SetInt(ConstantsInGame.Coin, coin);
        PlayerPrefs.SetString(ConstantsInGame.Chest, DateTime.Now.ToString());

        panel_result.SetActive(true);
        label_result.gameObject.SetActive(true);
        label_result.text = pieces.Amount.ToString();

        GameManager.Instance.CheckSetup();
        GameManager.Instance.GetComponent<GameServices>().RequestNotification("Check Your Game!" , "Dont forget to claim your reward today!" , 2);
    }

    private void ShowResult(string txt)
    {
        b_spin.gameObject.SetActive(false);
        label_result.gameObject.SetActive(true);
        label_result.text = txt;
    }

    private void HandleSpin()
    {
        buttonAnim.SetBool("isActive", false);
        b_spin.interactable = false;
        if (isAndroid)
        {
            AdsManager.Instance.RequestRewardedAds(AdsManager.DailySpin);
        }
        pickerWheel.Spin();
    }
}
